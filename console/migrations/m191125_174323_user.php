<?php

use yii\db\Migration;

/**
 * Class m191125_174323_user
 */
class m191125_174323_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191125_174323_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191125_174323_user cannot be reverted.\n";

        return false;
    }
    */
}
