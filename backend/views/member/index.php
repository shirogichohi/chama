<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Membership Form';
$this->params['breadcrumbs'][] = $this->title;
use yii\helpers\Url;
?>
<div class="member-index">
    <p>
        <?= Html::button('Add New Member', ['value' => Url::to(['member/create']), 'title' => 'Creating New Member', 'class' => 'showModalButton btn btn-primary']); ?>
    </p>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'fullname',
            'IDnumber',
            'phonenumber',
            'emailaddress:email',
            //'department_id',
            //'date_joined',

            ['class' => 'yii\grid\ActionColumn'],
            [
                'label' =>'Kins',
                'value' => function ($model)
                {
                    return Html::button('<span class=" glyphicon glyphicon-plus"></span>', ['value' => Url::to(['memberkin/create' , 'id'=> $model->member_id]), 'title' => 'Creating New Role', 'class' => 'showModalButton btn btn-info' ]);
                },
                'format'=>'raw',
            ],
            [
                'label' =>'Children',
                'value' => function ($model)
                {
                    return Html::button('<span class=" glyphicon glyphicon-plus"></span>', ['value' => Url::to(['memberkin/create' , 'id'=> $model->member_id]), 'title' => 'Creating New Role', 'class' => 'showModalButton btn btn-info' ]);
                },
                'format'=>'raw',
            ]
        ],
    ]); ?>
</div>
