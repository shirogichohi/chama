<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MemberChildrenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Member Childrens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-children-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Member Children', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'child_id',
            'fullname',
            'phonenumber',
            'emailaddress:email',
            'member_id',
            //'created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
