<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MemberChildrenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-children-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'child_id') ?>

    <?= $form->field($model, 'fullname') ?>

    <?= $form->field($model, 'phonenumber') ?>

    <?= $form->field($model, 'emailaddress') ?>

    <?= $form->field($model, 'member_id') ?>

    <?php // echo $form->field($model, 'created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
