<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MemberChildren */

$this->title = 'Update Member Children: ' . $model->child_id;
$this->params['breadcrumbs'][] = ['label' => 'Member Childrens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->child_id, 'url' => ['view', 'id' => $model->child_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="member-children-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
