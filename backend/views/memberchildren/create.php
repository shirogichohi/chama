<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MemberChildren */

$this->title = 'Add Member Children';
$this->params['breadcrumbs'][] = ['label' => 'Member Childrens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-children-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,

    ]) ?>

</div>
