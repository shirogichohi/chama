<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departments';
$this->params['breadcrumbs'][] = $this->title;
use yii\helpers\Url;
?>
<div class="department-index">

    <div class="">


    <p>
       <?= Html::button('Add New Department', ['value' => Url::to(['department/create']), 'title' => 'Creating New Department', 'class' => 'showModalButton btn btn-primary']); ?>

    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           'department',
            [
                'label' =>'Action',
                'value' => function ($model)
                {
                    return Html::button('<span class=" glyphicon glyphicon-pencil"></span>', ['value' => Url::to(['department/update' , 'id'=> $model->department_id]), 'title' => 'Creating New Role', 'class' => 'showModalButton btn btn-info' ])
                        .''.Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->department_id], [ 'class' => 'btn btn-danger' , 'data' => ['confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),'method' => 'post',],]);
                },
                'format'=>'raw',
            ]
        ],
    ]); ?>

    </div>
</div>
