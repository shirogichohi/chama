<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p> <?= \Yii::$app->user->identity->username ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/']],
                    [
                        'label' => 'Settings',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Company Details', 'icon' => 'file-code-o', 'url' => ['/client'],],
                            ['label' => 'User Roles', 'icon' => 'file-code-o', 'url' => ['/role'],],
                            ['label' => 'Departments', 'icon' => 'dashboard', 'url' => ['/department'],],
                            ['label' => 'Membership Settings', 'icon' => 'dashboard', 'url' => ['/department'],],
                    ]
                    ],
                    ['label' => 'Users', 'icon' => 'dashboard', 'url' => ['/site/users']],
                    [
                        'label' => 'Members',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Transfers', 'icon' => 'file-code-o', 'url' => ['/role'],],
                            ['label' => 'Cases', 'icon' => 'dashboard', 'url' => ['/department'],],
                            ['label' => 'Wallet', 'icon' => 'dashboard', 'url' => ['/department'],],
                        ]
                    ],
                    //representatives
                    ['label' => 'Department Cases', 'options' => ['class' => 'header']],
                    ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/']],
                    [
                        'label' => 'Members',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'List of Members', 'icon' => 'file-code-o', 'url' => ['/member'],],
                            ['label' => 'Transfers', 'icon' => 'file-code-o', 'url' => ['/role'],],
                            ['label' => 'Members Wallet', 'icon' => 'dashboard', 'url' => ['/department'],],
                        ]
                    ],
                    [
                        'label' => 'Cases',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'New Case', 'icon' => 'file-code-o', 'url' => ['/'],],
                            ['label' => 'Closed Cases', 'icon' => 'file-code-o', 'url' => ['/role'],],
                            ['label' => 'Pending Cases', 'icon' => 'dashboard', 'url' => ['/department'],],
                        ]
                    ],
                    ['label' => 'Department Wallet', 'icon' => 'dashboard', 'url' => ['/department']],
                    [
                        'label' => 'Reports',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Members', 'icon' => 'file-code-o', 'url' => ['/'],],
                            ['label' => 'Closed Cases', 'icon' => 'file-code-o', 'url' => ['/role'],],
                            ['label' => 'Members Wallet', 'icon' => 'dashboard', 'url' => ['/department'],],
                        ]
                    ],
                    //Treasurer
                    ['label' => 'Treasurer', 'options' => ['class' => 'header']],
                    ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/']],
                    ['label' => 'Members', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Membership Funds', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Case Funds', 'icon' => 'dashboard', 'url' => ['/debug']],
                    //Treasurer
                    ['label' => 'Treasurer', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>
    </section>

</aside>
