<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <p>
        <?= Html::button('Add New User', ['value' => Url::to(['site/register']), 'title' => 'Creating New User', 'class' => 'showModalButton btn btn-primary']); ?>

    </p>

    <div class="row">
        <div class="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'fullname',
                    'username','email','phonenumber','role',
                    [
                        'label' =>'Action',
                        'value' => function ($model)
                        {
                            return Html::button('<span class=" glyphicon glyphicon-pencil"></span>', ['value' => Url::to(['site/update' , 'id'=> $model->id]), 'title' => 'Creating New Role', 'class' => 'showModalButton btn btn-info' ])
                                .''.Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [ 'class' => 'btn btn-danger' , 'data' => ['confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),'method' => 'post',],]);
                        },
                        'format'=>'raw',
                    ]
                ],
            ]); ?>
        </div>
    </div>
</div>
