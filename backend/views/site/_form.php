<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Role;
use app\models\Department;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="signup-form">

    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <?= $form->field($model, 'fullname')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'phonenumber')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'email') ?>
    <div id="role">
    <?=
    $form->field($model, 'role')
        ->dropDownList(
            ArrayHelper::map(Role::find()->asArray()->all(), 'role', 'role'),  array('onchange'=>'return checkStatus(this.value)', 'prompt'=>'Select Role')
        )
    ?>
    </div>
    <div id="showDiv">
        <?=
        $form->field($model, 'department')
            ->dropDownList(
                ArrayHelper::map(Department::find()->asArray()->all(), 'department_id', 'department'),  array('onchange'=>'return checkStatus(this.value)', 'class' =>'form-control hidding', 'prompt'=>'Select Department')
            )
        ?>
    </div>


    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
