<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MemberKinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Member Kins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-kin-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Member Kin', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kin_id',
            'member_id',
            'kin_fullname',
            'kin_address',
            'kin_emailaddress:email',
            //'kin_phonenumber',
            //'created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
