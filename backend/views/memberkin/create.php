<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MemberKin */

$this->title = 'Add Member Kin';
$this->params['breadcrumbs'][] = ['label' => 'Member Kins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-kin-create">
    <?= $this->render('_form', [
        'model' => $model, 'id' => $id, 'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
