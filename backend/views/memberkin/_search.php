<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MemberKinSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-kin-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'kin_id') ?>

    <?= $form->field($model, 'member_id') ?>

    <?= $form->field($model, 'kin_fullname') ?>

    <?= $form->field($model, 'kin_address') ?>

    <?= $form->field($model, 'kin_emailaddress') ?>

    <?php // echo $form->field($model, 'kin_phonenumber') ?>

    <?php // echo $form->field($model, 'created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
