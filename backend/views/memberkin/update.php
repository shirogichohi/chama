<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MemberKin */

$this->title = 'Update Member Kin: ';
$this->params['breadcrumbs'][] = ['label' => 'Member Kins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kin_id, 'url' => ['view', 'id' => $model->kin_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="member-kin-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
