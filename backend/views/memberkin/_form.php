<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MemberKin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-kin-form">

    <?php $form = ActiveForm::begin(); ?>
<?php if(isset($id)) {?>
    <?=  $form->field($model, 'member_id')->hiddenInput(['value'=> $id])->label(false); ?>
    <?php } else {
    $form->field($model, 'member_id')->textInput()->label(false);
} ?>

    <?= $form->field($model, 'kin_fullname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kin_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kin_emailaddress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kin_phonenumber')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
<?php if(isset($dataProvider) ) {?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kin_fullname',
            'kin_address',
            'kin_emailaddress:email',
            'kin_phonenumber',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php } ?>
</div>
