<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Company Details';
$this->params['breadcrumbs'][] = $this->title;
use yii\helpers\Url;
?>
<div class="client-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'description',
            'address',
            'phonenumber',
            'emailaddress:email',
           [
               'label' =>'Action',
               'value' => function ($model)
               {
                   return Html::button('<span class=" glyphicon glyphicon-pencil"></span>', ['value' => Url::to(['client/update' , 'id'=> $model->client_id]), 'title' => 'Update', 'class' => 'showModalButton btn btn-info' ]);
               },
               'format'=>'raw',
           ]
        ],
    ]); ?>


</div>
