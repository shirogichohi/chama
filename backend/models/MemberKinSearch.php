<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MemberKin;

/**
 * MemberKinSearch represents the model behind the search form of `app\models\MemberKin`.
 */
class MemberKinSearch extends MemberKin
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kin_id', 'member_id'], 'integer'],
            [['kin_fullname', 'kin_address', 'kin_emailaddress', 'kin_phonenumber', 'created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MemberKin::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kin_id' => $this->kin_id,
            'member_id' => $this->member_id,
            'created' => $this->created,
        ]);

        $query->andFilterWhere(['like', 'kin_fullname', $this->kin_fullname])
            ->andFilterWhere(['like', 'kin_address', $this->kin_address])
            ->andFilterWhere(['like', 'kin_emailaddress', $this->kin_emailaddress])
            ->andFilterWhere(['like', 'kin_phonenumber', $this->kin_phonenumber]);

        return $dataProvider;
    }
}
