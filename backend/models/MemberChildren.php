<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_children".
 *
 * @property int $child_id
 * @property string $fullname
 * @property string $phonenumber
 * @property string $emailaddress
 * @property int $member_id
 * @property string $created
 *
 * @property Member $member
 */
class MemberChildren extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member_children';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'phonenumber', 'emailaddress', 'member_id'], 'required'],
            [['member_id'], 'integer'],
            [['created'], 'safe'],
            [['fullname', 'phonenumber', 'emailaddress'], 'string', 'max' => 200],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => Member::className(), 'targetAttribute' => ['member_id' => 'member_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'child_id' => 'Child ID',
            'fullname' => 'Fullname',
            'phonenumber' => 'Phonenumber',
            'emailaddress' => 'Emailaddress',
            'member_id' => 'Member ID',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::className(), ['member_id' => 'member_id']);
    }
}
