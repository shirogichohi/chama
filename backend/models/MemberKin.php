<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_kin".
 *
 * @property int $kin_id
 * @property int $member_id
 * @property string $kin_fullname
 * @property string $kin_address
 * @property string $kin_emailaddress
 * @property string $kin_phonenumber
 * @property string $created
 *
 * @property Member $member
 */
class MemberKin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member_kin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'kin_fullname', 'kin_address', 'kin_emailaddress', 'kin_phonenumber'], 'required'],
            [['member_id'], 'integer'],
            [['created'], 'safe'],
            [['kin_fullname', 'kin_address', 'kin_emailaddress', 'kin_phonenumber'], 'string', 'max' => 200],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => Member::className(), 'targetAttribute' => ['member_id' => 'member_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kin_id' => 'Kin ID',
            'member_id' => 'Member ID',
            'kin_fullname' => 'Kin Fullname',
            'kin_address' => 'Kin Address',
            'kin_emailaddress' => 'Kin Emailaddress',
            'kin_phonenumber' => 'Kin Phonenumber',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::className(), ['member_id' => 'member_id']);
    }
}
