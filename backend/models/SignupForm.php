<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $phonenumber;
    public $role;
    public $fullname;
    public $department;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['phonenumber', 'trim'],
            ['phonenumber', 'required'],
            ['phonenumber', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This phonenumber has already been saved.'],
            ['phonenumber', 'string', 'min' => 2, 'max' => 255],

            ['fullname', 'trim'],
            ['fullname', 'required'],
            ['fullname', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This Name has already been taken.'],
            ['fullname', 'string', 'min' => 2, 'max' => 255],

            ['role', 'trim'],
            ['role', 'required'],

            ['department', 'trim'],
            ['department', 'required'],

            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->fullname = $this->fullname;
        $user->phonenumber = $this->phonenumber;
        $user->department = $this->department;
        $user->role = $this->role;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        return $user->save() && $this->sendEmail($user);

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
