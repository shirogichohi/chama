<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property int $member_id
 * @property string $fullname
 * @property string $IDnumber
 * @property string $phonenumber
 * @property string $emailaddress
 * @property int $department_id
 * @property string $date_joined
 *
 * @property Cases[] $cases
 * @property MemberChildren[] $memberChildrens
 * @property MemberKin[] $memberKins
 * @property Transfer[] $transfers
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'IDnumber', 'phonenumber', 'emailaddress', 'department_id'], 'required'],
            [['department_id'], 'integer'],
            [['date_joined'], 'safe'],
            [['fullname', 'emailaddress'], 'string', 'max' => 250],
            [['IDnumber', 'phonenumber'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'member_id' => 'Member ID',
            'fullname' => 'Fullname',
            'IDnumber' => 'I Dnumber',
            'phonenumber' => 'Phonenumber',
            'emailaddress' => 'Emailaddress',
            'department_id' => 'Department ID',
            'date_joined' => 'Date Joined',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCases()
    {
        return $this->hasMany(Cases::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberChildrens()
    {
        return $this->hasMany(MemberChildren::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberKins()
    {
        return $this->hasMany(MemberKin::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransfers()
    {
        return $this->hasMany(Transfer::className(), ['member_id' => 'member_id']);
    }
}
