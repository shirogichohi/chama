<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MemberChildren;

/**
 * MemberChildrenSearch represents the model behind the search form of `app\models\MemberChildren`.
 */
class MemberChildrenSearch extends MemberChildren
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['child_id', 'member_id'], 'integer'],
            [['fullname', 'phonenumber', 'emailaddress', 'created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MemberChildren::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'child_id' => $this->child_id,
            'member_id' => $this->member_id,
            'created' => $this->created,
        ]);

        $query->andFilterWhere(['like', 'fullname', $this->fullname])
            ->andFilterWhere(['like', 'phonenumber', $this->phonenumber])
            ->andFilterWhere(['like', 'emailaddress', $this->emailaddress]);

        return $dataProvider;
    }
}
