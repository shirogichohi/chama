<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client".
 *
 * @property int $client_id
 * @property string $description
 * @property string $address
 * @property string $phonenumber
 * @property string $emailaddress
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'address', 'phonenumber', 'emailaddress'], 'required'],
            [['description', 'address', 'phonenumber', 'emailaddress'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Client ID',
            'description' => 'Description',
            'address' => 'Address',
            'phonenumber' => 'Phonenumber',
            'emailaddress' => 'Emailaddress',
        ];
    }
}
